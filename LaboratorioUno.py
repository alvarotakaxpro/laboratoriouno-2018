#Ejercicio 1:Desarrollar un programa que ingrese número y determine si es número par o impar

"""numero=int(input("Ingrese un numero para determinar su es par o impar"))

if numero%2 == 0:
    print("par")
else:
    print("impar")"""

#Ejercicio 2: Escriba un programa que pida el año actual y un año cualquiera y que escriba cuántos años han
#pasado desde ese año o cuántos años faltan para llegar a ese año.

"""ño=int(input("Ingrese un año"))

if año == 2018:
    print("El año ingresado :", año, ", es igual al año actual 2018.")
elif año>2018:
    calculo=año-2018
    print("para llegar al año solicitado faltan : ", calculo, " año (s)")
else:
    calculo=2018-año
    print("el año ingresado (", año, "), fue hace: ", calculo, " para llegar al 2018.")"""

#Ejercicio 3: Cree un programa que pida al usuario su edad y muestre por pantalla la etapa en la que se
#encuentre. (Python -Diagrama) 7 puntos.
#a. 0 a 10 niño(@)
#b. 11 a 18 adolescente
#c. 19 a 64 adulto
#d. 65 en adelante Adulto Mayor

"""edad=int(input("Ingrese la edad"))
if edad >= 0 and edad <= 10:
    print("es un niñ@")
elif edad>=11 and edad<=18:
    print("Es Adolescente")
else:
    if edad>=19 and edad<=64:
        print("Es Adulto ")
    elif edad>=65:
        print("Es Adulto Mayor")
    else:
        print("Digite una edad válida!!!")
print("Gracias!")"""

#Ejercicio 4: Imprima los números del 1 al 20 usando un ciclo while

"""numero=0
while numero<20:
    numero=numero+1
    print(numero)"""

#Ejercicio 5= Imprima los números del 20 al 1 usando un ciclo for

"""for i in range(20, 0, -1):
    print(i)"""

#Ejercicio 5= Escriba un programa de alquiler de vehículos que le permita al usuario seleccionar una categoría y
#un modelo de vehículo por una cantidad de días e imprima el monto a pagar a la agencia, el sistema
#debe identificar si el cliente es frecuente o no para aplicarle un descuento y debe cobrar una póliza
#de seguro, además si el cliente requiere el vehículo más tiempo del establecido el costo por día tendrá
#un incremento dependiendo del tipo de vehículo, tenga en cuenta los siguientes parámetros:
#Clase           |    Modelo        | DíasmáximoprecioNormal  | Preciopordía    | Preciopordíaextra  | Póliza  | DescuentoClienteFrecuente
#Automóvil       |   >= 2015        |        7                |     13,000.00   |      15%           |  10%    |       3%
#Automóvil       |   >=2001 < 2014  |        5                |     18,000.00   |      10%           |  10%    |       3%
#Automóvil       |   >=1990 <2000   |        7                |     10,000.00   |      5%            |  30%    |      10%
#Hatchback       |   >= 2015        |        8                |     20,000.00   |      10%           |   8%    |       5%
#Hatchback       |   >=2000 < 2014  |        6                |     17,000.00   |       5%           |   9%    |       3%
#Doble Tracción  |   > 2015         |        7                |     20,000.00   |      15%           |  11%    |      10%
#Doble Tracción  |   > 2000 <= 2015 |        4                |     16,000.00   |       5%           |  10%    |       5%
#***** El usuario no puede seleccionar ningún modelo que no exista en la agencia


print("Bienvenidos a Rent a Carl")
print("Por Favor seleccione el numero de una de las siguientes clases de Vehiculos: \n"
                "Opcion| CLASE           |    MODELO        | DíasmáximoprecioNormal  | Preciopordía    | Preciopordíaextra  | Póliza  \n"
                "   1  | Automóvil       |   >= 2015        |        7                |   13,000.00     |      15%           |  10%    \n"
                "   1  | Automóvil       |   >=2001 < 2014  |        5                |   18,000.00     |      10%           |  10%    \n"
                "   1  | Automóvil       |   >=1990 <2000   |        7                |   10,000.00     |      5%            |  30%    \n"
                "   2  | Hatchback       |   >= 2015        |        8                |   20,000.00     |      10%           |   8%    \n"
                "   2  | Hatchback       |   >=2000 < 2014  |        6                |   17,000.00     |      5%            |   9%    \n"
                "   3  | Doble Tracción  |   > 2015         |        7                |   20,000.00     |      15%           |  11%    \n"
                "   3  | Doble Tracción  |   > 2000 <= 2015 |        4                |   16,000.00     |      5%            |  10%    \n")
opcion=int(input("Ingrese la Opcion que desea elegir : "))

modelo=int(input("Ingrese el modelo del vehiculo"))

pde=int(input("Cantidad de Dias que desea alquilar el vehiculo : "))

dcf=int(input("¿Usted es cliente Frecuente:\n"
              "1- Si \n"
              "2- No \n"))

if opcion==1:
    if modelo>=2015:
      if pde>7:
        diasextras=pde-7
        preciodiaextra=diasextras*14950
        bruto=91000+preciodiaextra
        poliza=bruto*0.1
        brutopoliza=bruto-poliza
        if dcf==1:
            diasextras=0
            preciodiaextra=0
            brutofrecuente=brutopoliza*0.03
            neto=brutopoliza-brutofrecuente
            print("Has seleccionado Vehiculo Automovil modelo 2015 o superior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)
        elif dcf==2 :
            brutofrecuente=0
            neto=brutopoliza
            print("Has seleccionado Vehiculo Automovil modelo 2015 o superior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)



      elif pde>0 and pde<=7:
        bruto=pde*13000
        poliza=bruto*0.1
        brutopoliza=bruto-poliza

        if dcf==1:
            diasextras=0
            preciodiaextra=0
            brutofrecuente=brutopoliza*0.03
            neto=brutopoliza-brutofrecuente
            print("Has seleccionado Vehiculo Automovil modelo 2015 o superior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)
        elif dcf==2 :
            brutofrecuente=0
            neto=brutopoliza
            preciodiaextra=0
            diasextras=0
            print("Has seleccionado Vehiculo Automovil modelo 2015 o superior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)

    elif modelo>=2001 and modelo<=2014:
        if pde>5:
         diasextras=pde-5
         preciodiaextra=diasextras*19800
         bruto=90000+preciodiaextra
         poliza=bruto*0.1
         brutopoliza=bruto-poliza
         if dcf==1:
            diasextras=0
            preciodiaextra=0
            brutofrecuente=brutopoliza*0.03
            neto=brutopoliza-brutofrecuente
            print("Has seleccionado Vehiculo Automovil modelo 2015 o superior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)
         elif dcf==2 :
            brutofrecuente=0
            neto=brutopoliza
            print("Has seleccionado Vehiculo Automovil modelo 2014 o inferior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)



        elif pde>0 and pde<=5:
         bruto=pde*18000
         poliza=bruto*0.1
         brutopoliza=bruto-poliza
         if dcf==1:
            diasextras=0
            preciodiaextra=0
            brutofrecuente=brutopoliza*0.03
            neto=brutopoliza-brutofrecuente
            print("Has seleccionado Vehiculo Automovil modelo 2014 o inferior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)
         elif dcf==2 :
            brutofrecuente=0
            neto=brutopoliza
            preciodiaextra=0
            diasextras=0
            print("Has seleccionado Vehiculo Automovil modelo 2015 o superior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)
    else:
        if modelo>=1990 and modelo<=2000:
            if pde>7:
              diasextras=pde-7
              preciodiaextra=diasextras*73500
              bruto=70000+preciodiaextra
              poliza=bruto*0.3
              brutopoliza=bruto-poliza
              if dcf==1:
                diasextras=0
                preciodiaextra=0
                brutofrecuente=brutopoliza*0.1
                neto=brutopoliza-brutofrecuente
                print("Has seleccionado Vehiculo Automovil modelo 2015 o superior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)
              elif dcf==2 :
                 brutofrecuente=0
                 neto=brutopoliza
                 print("Has seleccionado Vehiculo Automovil modelo 2014 o inferior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)
            elif pde>0 and pde<=7:
              bruto=pde*10000
              poliza=bruto*0.3
              brutopoliza=bruto-poliza
              if dcf==1:
                diasextras=0
                preciodiaextra=0
                brutofrecuente=brutopoliza*0.1
                neto=brutopoliza-brutofrecuente
                print("Has seleccionado Vehiculo Automovil modelo 2014 o inferior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)
              elif dcf==2:
                brutofrecuente=0
                neto=brutopoliza
                preciodiaextra=0
                diasextras=0
                print("Has seleccionado Vehiculo Automovil modelo 2015 o superior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)
        else:
                print("no existe")
elif opcion==2:
  if modelo>=2015:
      if pde>7:
        diasextras=pde-7
        preciodiaextra=diasextras*14950
        bruto=91000+preciodiaextra
        poliza=bruto*0.1
        brutopoliza=bruto-poliza
        if dcf==1:
            diasextras=0
            preciodiaextra=0
            brutofrecuente=brutopoliza*0.03
            neto=brutopoliza-brutofrecuente
            print("Has seleccionado Vehiculo Automovil modelo 2015 o superior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)
        elif dcf==2 :
            brutofrecuente=0
            neto=brutopoliza
            print("Has seleccionado Vehiculo Automovil modelo 2015 o superior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)



      elif pde>0 and pde<=7:
        bruto=pde*13000
        poliza=bruto*0.1
        brutopoliza=bruto-poliza

        if dcf==1:
            diasextras=0
            preciodiaextra=0
            brutofrecuente=brutopoliza*0.03
            neto=brutopoliza-brutofrecuente
            print("Has seleccionado Vehiculo Automovil modelo 2015 o superior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)
        elif dcf==2 :
            brutofrecuente=0
            neto=brutopoliza
            preciodiaextra=0
            diasextras=0
            print("Has seleccionado Vehiculo Automovil modelo 2015 o superior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)

  elif modelo>=2001 and modelo<=2014:
        if pde>5:
         diasextras=pde-5
         preciodiaextra=diasextras*19800
         bruto=90000+preciodiaextra
         poliza=bruto*0.1
         brutopoliza=bruto-poliza
         if dcf==1:
            diasextras=0
            preciodiaextra=0
            brutofrecuente=brutopoliza*0.03
            neto=brutopoliza-brutofrecuente
            print("Has seleccionado Vehiculo Automovil modelo 2015 o superior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)
         elif dcf==2 :
            brutofrecuente=0
            neto=brutopoliza
            print("Has seleccionado Vehiculo Automovil modelo 2014 o inferior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)



        elif pde>0 and pde<=5:
         bruto=pde*18000
         poliza=bruto*0.1
         brutopoliza=bruto-poliza
         if dcf==1:
            diasextras=0
            preciodiaextra=0
            brutofrecuente=brutopoliza*0.03
            neto=brutopoliza-brutofrecuente
            print("Has seleccionado Vehiculo Automovil modelo 2014 o inferior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)
         elif dcf==2 :
            brutofrecuente=0
            neto=brutopoliza
            preciodiaextra=0
            diasextras=0
            print("Has seleccionado Vehiculo Automovil modelo 2015 o superior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)

else:
    if opcion==3:
        if modelo>=2015:
          if pde>7:
            diasextras=pde-7
            preciodiaextra=diasextras*14950
            bruto=91000+preciodiaextra
            poliza=bruto*0.1
            brutopoliza=bruto-poliza
            if dcf==1:
              diasextras=0
              preciodiaextra=0
              brutofrecuente=brutopoliza*0.03
              neto=brutopoliza-brutofrecuente
              print("Has seleccionado Vehiculo Automovil modelo 2015 o superior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)
            elif dcf==2 :
              brutofrecuente=0
              neto=brutopoliza
              print("Has seleccionado Vehiculo Automovil modelo 2015 o superior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)



          elif pde>0 and pde<=7:
               bruto=pde*13000
               poliza=bruto*0.1
               brutopoliza=bruto-poliza

               if dcf==1:
                 diasextras=0
                 preciodiaextra=0
                 brutofrecuente=brutopoliza*0.03
                 neto=brutopoliza-brutofrecuente
                 print("Has seleccionado Vehiculo Automovil modelo 2015 o superior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)
               elif dcf==2 :
                 brutofrecuente=0
                 neto=brutopoliza
                 preciodiaextra=0
                 diasextras=0
                 print("Has seleccionado Vehiculo Automovil modelo 2015 o superior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)

        elif modelo>=2001 and modelo<=2014:
         if pde>5:
           diasextras=pde-5
           preciodiaextra=diasextras*19800
           bruto=90000+preciodiaextra
           poliza=bruto*0.1
           brutopoliza=bruto-poliza
           if dcf==1:
              diasextras=0
              preciodiaextra=0
              brutofrecuente=brutopoliza*0.03
              neto=brutopoliza-brutofrecuente
              print("Has seleccionado Vehiculo Automovil modelo 2015 o superior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)
           elif dcf==2 :
              brutofrecuente=0
              neto=brutopoliza
              print("Has seleccionado Vehiculo Automovil modelo 2014 o inferior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)



         elif pde>0 and pde<=5:
           bruto=pde*18000
           poliza=bruto*0.1
           brutopoliza=bruto-poliza
           if dcf==1:
             diasextras=0
             preciodiaextra=0
             brutofrecuente=brutopoliza*0.03
             neto=brutopoliza-brutofrecuente
             print("Has seleccionado Vehiculo Automovil modelo 2014 o inferior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)
           elif dcf==2 :
              brutofrecuente=0
              neto=brutopoliza
              preciodiaextra=0
              diasextras=0
              print("Has seleccionado Vehiculo Automovil modelo 2015 o superior \n"
                  "dias solicitados           : ", pde, "\n" 
                  "cantidad dias extras       : ", diasextras, "\n"
                  "precio dias extras         : ", preciodiaextra, "\n"
                  "Rebajo poliza              : ", poliza, "\n"
                  "Descuento Cliente Frecuente: ", brutofrecuente, "\n"
                  "TOTAL A PAGAR              : ", neto)





